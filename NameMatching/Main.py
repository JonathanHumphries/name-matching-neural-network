import TrainingData
import RegressionNeuralNetwork
import torch

def help():
    print("Train - will train the network")
    print("Quiz - ask the network to match a name")
    print("Exit - exits the program")

def train(td : TrainingData):
    inputSize = len(td.EncodedTrainingData[0]) - 1

	#this heuristic for hidden layer size approximation was developed by Jonathan
	#Humphries in fall of 2018
    encodedData = len(td.EncodedTrainingData) * inputSize
    predictedNodes = encodedData * 0.01048 + 4000
    hiddenSize = int((predictedNodes - 1 - inputSize) / (inputSize + 1))

    print("Creating a Regression Neural Network with Input Size: {} and Hidden Size: {}".format(inputSize, hiddenSize))
    net = RegressionNeuralNetwork.RegressionNeuralNetwork(inputSize, hiddenSize, 1)
    # net.load_state_dict(torch.load("NameMatchingModel.dat"))
    net =  torch.load("NameMatchingModel.dat")
    
    print("Training the neural network on {} rows of encoded data".format(len(td.TrainingX)))

	#train a maximum of 500000 iterations or until an accuracy of 80% is achieved,
	#whichever comes first
	#use a learning rate of 0.01
    net.Train(td, False, 500000, 0.01, 0.8)
	#save the model
    torch.save(net.state_dict(), "NameMatchingModel_SD.dat")
    torch.save(net, "NameMatchingModel.dat")

    print("Test the neural network on {} rows of encoded data".format(len(td.TestingX)))
    acc = net.Test(td)

    print("Accuracy on test data = {}".format(acc))

def quiz(trainingData : TrainingData):
    net = torch.load("NameMatchingModel.dat")
    net.eval()
    print("Enter a name")
    print(net.quiz(input(), trainingData))

def mainMenu(trainingData : TrainingData):
    exit = False
    while not exit:
        print("\r\nEnter a command or type 'Help' for a list of commands")	
        cmd = input().lower()
        if (cmd == "exit") : exit = True
        elif (cmd == "quiz") : quiz(trainingData)
        elif (cmd == "train") : train(trainingData)
        else : help()

def main():
    print("Welcome to a simple name matching demo using pytorch as a 2 layer feedforward fully connected neural network")
    print("Loading training data into memory")
    td = TrainingData.TrainingData()
    td.LoadTrainingData("./Data/PayerMlData.csv",',', 2, .25)

    print("Loading complete")
   
    mainMenu(td)

main()