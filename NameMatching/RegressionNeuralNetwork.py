import numpy as np
import StringComparisonEncoding
import torch as T
import TrainingData

# Defines pyTorch instance of my regression neural network structure
# Original Reference https://msdn.microsoft.com/en-us/magazine/mt848704.aspx
class RegressionNeuralNetwork(T.nn.Module):

	#Generate a standard 2 layer feed forward neural network and initialize the
	#weights with the glorot method
    def __init__(self, inputSize, hiddenSize, outputSize):
        super(RegressionNeuralNetwork, self).__init__()
        self.InputSize = inputSize
        self.HiddenSize = hiddenSize
        self.OutputSize = outputSize
        self.fc1 = T.nn.Linear(self.InputSize, self.HiddenSize)
        T.nn.init.xavier_uniform_(self.fc1.weight)  # glorot
        T.nn.init.zeros_(self.fc1.bias)
        self.fc2 = T.nn.Linear(self.HiddenSize, self.OutputSize)
        T.nn.init.xavier_uniform_(self.fc2.weight)
        T.nn.init.zeros_(self.fc2.bias)

	#Set the hidden layer activation method
    def forward(self, x):
        z = T.tanh(self.fc1(x))
        z = self.fc2(z)  # see CrossEntropyLoss() below
        return z

    #Used to check values in a trained model and return the best match
    def quiz(self, x, trainingData: TrainingData.TrainingData):
        best = "Not Found"
        bestScore = 0
        for payer in trainingData.MatchingList:
            X = T.Tensor(StringComparisonEncoding.EncodeStrings(x, payer))
            oupt = self(X)       # predicted as [102,1] Tensor
            pred = oupt.view(1).item()  # predicted as [102]
            if pred > bestScore: 
                bestScore = pred
                best = payer
                print(payer + " " + str(pred))
        return best

	# this method was customized for the name matching algorithm but
	# the original reference is
	# https://jamesmccaffrey.wordpress.com/2018/11/26/an-efficient-accuracy-function-for-pytorch-regression/
    def accuracy(self, data_x, data_y, pct_close):
		# pure Tensor, efficient version
        n_items = len(data_y)
        X = T.Tensor(data_x)
        Y = T.Tensor(data_y)  # actual as [102] Tensor
        
        oupt = self(X)       # predicted as [102,1] Tensor
        pred = oupt.view(n_items)  # predicted as [102]
        
        n_correct = 0
        for i in range(0,len(pred)): 
            # if actual is 1 and difference is .2 or less (80%) then its a match.
            if (Y[i] == 1 and abs(pred[i] - Y[i]) < .2) : n_correct = n_correct + 1
            # if actual is 0 and difference is .8 or less then its a match
            if (Y[i] == 0 and abs(pred[i] - Y[i]) < .8): n_correct = n_correct + 1

		# n_correct = T.sum(T.abs(pred - Y) < pct_close) #McCaffrey's code looks
		# good, but the values returned were incorrect.
		# print("Pred Items: {} Num Correct: {} Num Items: {}".format(len(pred),
		# n_correct, n_items))
        acc = (n_correct / n_items)  # scalar
        return acc 

	# Trains this network using the training data supplied
	# quiet turns off print statements
    def Train(self, trainingData, quiet, maxIterations, learningRate, term_Accuracy):
        self = self.train()  # set training mode
        self.trainingData = trainingData
        n_items = len(trainingData.TrainingX)
        b_size = int(len(trainingData.TrainingX) * .1) # batches will be 1/10 the size of the collection
        loss_func = T.nn.L1Loss() # note loss function changed due to regression over categorization
        optimizer = T.optim.SGD(self.parameters(), lr=learningRate)
        batcher = Batch(num_items=n_items, bat_size=b_size)
        for i in range(0, maxIterations):
			# Write some stuff to the screen and look for a termination condition every
			# 100 iterations
            if i > 0 and i % (100) == 0:
                if (quiet == False): print("iteration = %4d" % i, end="")
                if (quiet == False): print("  loss = %7.4f" % loss_obj.item(), end="")
                acc = self.accuracy(trainingData.TrainingX, trainingData.TrainingY, 0.2)
                if (quiet == False): print("  accuracy = {0:.2f}%".format(acc * 100))
                if (acc >= term_Accuracy): break

            #use Stochastic Gradient Descent (SGD) to train the weights and biases
            curr_bat = batcher.next_batch()
            X = T.Tensor(trainingData.TrainingX[curr_bat])
            Y = T.Tensor(trainingData.TrainingY[curr_bat])
            oupt = self(X)
            loss_obj = loss_func(oupt, Y)
            optimizer.zero_grad()			
            loss_obj.backward()
            optimizer.step()

    def Test(self, trainingData):
        self = self.eval()
        return self.accuracy(trainingData.TestingX, trainingData.TestingY, 0.1)

# Sets up a randomly selected batch of training data
class Batch:
  
	def __init__(self, num_items, bat_size, seed=0):
		self.num_items = num_items
		self.bat_size = bat_size
		self.rnd = np.random.RandomState(seed)

	def next_batch(self):
		return self.rnd.choice(self.num_items, self.bat_size, replace=False)