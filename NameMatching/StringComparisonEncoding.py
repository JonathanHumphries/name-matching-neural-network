# Written by Jonathan Humphries Jan 2019

import numpy as np

# original reference: https://www.dotnetperls.com/levenshtein
# Python reference:
# https://stackabuse.com/levenshtein-distance-and-text-similarity-in-python/
def LevenshteinCompare(seq1, seq2):  
    size_x = len(seq1) + 1
    size_y = len(seq2) + 1
    matrix = np.zeros((size_x, size_y))
    for x in range(size_x):
        matrix[x, 0] = x
    for y in range(size_y):
        matrix[0, y] = y

    for x in range(1, size_x):
        for y in range(1, size_y):
            if seq1[x - 1] == seq2[y - 1]:
                matrix[x,y] = min(matrix[x - 1, y] + 1,
                    matrix[x - 1, y - 1],
                    matrix[x, y - 1] + 1)
            else:
                matrix[x,y] = min(matrix[x - 1,y] + 1,
                    matrix[x - 1,y - 1] + 1,
                    matrix[x,y - 1] + 1)
    #print (matrix)
    return (matrix[size_x - 1, size_y - 1])

# soundex reference: https://en.wikipedia.org/wiki/Soundex
# python reference:
# https://medium.com/@yash_agarwal2/soundex-and-levenshtein-distance-in-python-8b4b56542e9e
def Soundex(query: str):
    # Step 0: Clean up the query string
    query = query.lower()
    letters = [char for char in query if char.isalpha()]

    # Added by JOH in Jan 2019 for when str has no letters at
    if len(letters) == 0:
        return "0000"

    # Step 1: Save the first letter.  Remove all occurrences of a, e, i, o, u,
    # y, h, w.

    # If query contains only 1 letter, return query+"000" (Refer step 5)
    if len(query) == 1:
        return query + "000"

    to_remove = ('a', 'e', 'i', 'o', 'u', 'y', 'h', 'w')

    first_letter = letters[0]
    letters = letters[1:]
    letters = [char for char in letters if char not in to_remove]

    if len(letters) == 0:
        return first_letter + "000"

    # Step 2: Replace all consonants (include the first letter) with digits
    # according to rules

    to_replace = {('b', 'f', 'p', 'v'): 1, ('c', 'g', 'j', 'k', 'q', 's', 'x', 'z'): 2,
                  ('d', 't'): 3, ('l',): 4, ('m', 'n'): 5, ('r',): 6}

    first_letter = [value if first_letter else first_letter for group, value in to_replace.items()
                    if first_letter in group]
    letters = [value if char else char
               for char in letters
               for group, value in to_replace.items()
               if char in group]

    # Step 3: Replace all adjacent same digits with one digit.
    letters = [char for ind, char in enumerate(letters)
               if (ind == len(letters) - 1 or (ind + 1 < len(letters) and char != letters[ind + 1]))]

    # Step 4: If the saved letter’s digit is the same the resulting first
    # digit, remove the digit (keep the letter)
    if first_letter == letters[0]:
        letters[0] = query[0]
    else:
        letters.insert(0, query[0])

    # Step 5: Append 3 zeros if result contains less than 3 digits.
    # Remove all except first letter and 3 digits after it.

    first_letter = letters[0]
    letters = letters[1:]

    letters = [char for char in letters if isinstance(char, int)][0:3]

    while len(letters) < 3:
        letters.append(0)

    letters.insert(0, first_letter)

    string = "".join([str(l) for l in letters])

    return string

# Encodes a soundex string as a vector
def EncodeSoundex(str):
	return [ord(str[0:1].upper()), int(str[1:2]), int(str[2:3]), int(str[3:4])]

# Returns 0 if string 1 contains string 2 and 1 if it does not.  I prefer 0/1
# for binary encoding in NN
def EncodeStr1ContainsStr2(str1, str2):
	if str1.upper().find(str2.upper()) == -1: return 0
	else: return 1

# Returns the number of spaces in the string
def EncodeNumberOfSpace(str):
	return str.count(' ')

# Returns the length of the string
def EncodeLength(str):
	return len(str)

# Builds the fully encoded vector to be sent to the neural network
def CreateEncodedVectorY(es1, es2, c1, c2, lc, ns1, ns2, len1, len2, y):
	return [es1[0], es1[1], es1[2], es1[3], es2[0], es2[1], es2[2], es2[3], c1, c2, lc, ns1, ns2, len1, len2, y]

# Builds the fully encoded vector to be sent to the neural network
def CreateEncodedVector(es1, es2, c1, c2, lc, ns1, ns2, len1, len2):
	return [es1[0], es1[1], es1[2], es1[3], es2[0], es2[1], es2[2], es2[3], c1, c2, lc, ns1, ns2, len1, len2]

# Simplifies the Encoding process by returning the fully encoded vector from
# two string parameters
def EncodeStringsY(string1, string2, y):
	es1 = EncodeSoundex(Soundex(string1)) 
	es2 = EncodeSoundex(Soundex(string2)) 
	lc = int(LevenshteinCompare(string1,string2))
	c1 = EncodeStr1ContainsStr2(string1,string2)
	c2 = EncodeStr1ContainsStr2(string2,string1) 
	ns1 = EncodeNumberOfSpace(string1) 
	ns2 = EncodeNumberOfSpace(string2)
	len1 = EncodeLength(string1)
	len2 = EncodeLength(string2) 
	return CreateEncodedVectorY(es1, es2, c1, c2, lc, ns1, ns2, len1, len2, y)

# Simplifies the Encoding process by returning the fully encoded vector from
# two string parameters
def EncodeStrings(string1, string2):
	es1 = EncodeSoundex(Soundex(string1)) 
	es2 = EncodeSoundex(Soundex(string2)) 
	lc = int(LevenshteinCompare(string1,string2))
	c1 = EncodeStr1ContainsStr2(string1,string2)
	c2 = EncodeStr1ContainsStr2(string2,string1) 
	ns1 = EncodeNumberOfSpace(string1) 
	ns2 = EncodeNumberOfSpace(string2)
	len1 = EncodeLength(string1)
	len2 = EncodeLength(string2) 
	return CreateEncodedVector(es1, es2, c1, c2, lc, ns1, ns2, len1, len2)


#some methods for testing and viewing
def Testing():
	word1 = "Aetna"
	word2 = "aetna North Carolina"

	print(word1)
	print(word2)
	print(Soundex(" $1"))
	print(EncodeStrings(word1,word2))

