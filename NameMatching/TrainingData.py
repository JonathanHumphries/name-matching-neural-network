import StringComparisonEncoding
import random
import numpy
import torch as T
from pathlib import Path

class TrainingData:

	# Loads the file into a list of encoded vectors
	# This has some customizations for the name matching project
	# Specifically, the training data only includes "good" rows and we want the
	# neural network to return something like a percent match
	# Therefore, a special method has also been added to generate "bad" rows.
	# n is the number of bad rows to create for every good row
	# splitPct is the percent of rows that should be used for testing
    def LoadTrainingData(self, filePath, delimiter, n, splitPct):
        self.Lines = []
        self.EncodedTrainingData = []
        self.MatchingList = [] #stores the list of values we are trying to match too.  Not necessary for
                               #training as much as for unknown values.
        i = 0
        with open(filePath) as f:
            for line in f:
                if line.find(delimiter) != -1: # If it doesn't contain the delimiter then we shouldn't load it
                    splitLine = self.Scrub(line).split(delimiter)
                    self.Append(splitLine[0], splitLine[1],1) # 1 means it is a match
                    if not splitLine[1] in self.MatchingList: self.MatchingList.append(splitLine[1])
                i = i + 1
				# if (i >= 200): break # Training data row limiter for debugging purposes
        self.AddBadRows(n)
        outputDataPosition = len(self.EncodedTrainingData[0]) - 1 # This is a bit overkill but will be useful as I make this class more generic
        self.TrainingX = []
        self.TrainingY = []
        self.TestingX = []
        self.TestingY = []
        self.SplitData(splitPct, outputDataPosition)
		# I'm not real fond of this next bit but I'd have to put more time into numpy
		# array creation to bypass it
        self.TrainingX = numpy.array(self.TrainingX)
        self.TrainingY = numpy.array(self.TrainingY)
        self.TestingX = numpy.array(self.TestingX)
        self.TestingY = numpy.array(self.TestingY)


	# perform any clean up on the row of text before encoding
    def Scrub(self, str):
        return str.replace('\n','')

	# return a simple matching on the two strings
	# this is not ideal but we don't have any bad records int the training data
    def SimpleMatch(self, str1, str2):
        return (str1.upper().find(str2.upper()) or str2.upper().find(str1.upper()) or StringComparisonEncoding.Soundex(str1) == StringComparisonEncoding.Soundex(str2))

	# look for rows where soundex is not equal and there are no good rows for that
	# value and set them with an expected response of 0
    def AddBadRows(self, n):
        maxLines = len(self.Lines)
        copyLines = self.Lines.copy() # without this it will loop forever
        for line in copyLines: # for every piece of training data
            for i in range(0,n): # generate n bad values
                doNotMatch = line[1] # this is the known good value so we don't want to confuse it by finding a
                         # string that is similar to this
                found = 1
                while (found == 1): # It's possible we accidentally grab a match so we will grab lines at random
                        # until we find one that fails the simple matching
                    linesIndex = random.randint(0, maxLines - 1)
                    nameToMatch = self.Lines[linesIndex][1]
                    match = self.SimpleMatch(nameToMatch, doNotMatch)
                    if (match == -1):
                        found = 0
                        self.Append(line[0], nameToMatch, 0) # 0 means it is not a match

	# Now that we have some strings, do all the appending to different lists...
    def Append(self, str1, str2, y):
        self.Lines.append([str1, str2, str(y)])
        self.EncodedTrainingData.append(StringComparisonEncoding.EncodeStringsY(str1, str2, y))

	# Split the data into Testing and Training data
    def SplitData(self, splitPct, outputDataPosition):
        for data in self.EncodedTrainingData:
            pct = random.uniform(0,1)
            if (pct < splitPct): # goes to Testing
                self.TestingX.append(numpy.array(data[0:outputDataPosition]))
                self.TestingY.append(data[outputDataPosition:len(data)])
            else: #goes to Training
                self.TrainingX.append(numpy.array(data[0:outputDataPosition]))
                self.TrainingY.append(data[outputDataPosition:len(data)])


def Testing():
	td = TrainingData()
	train_file = Path("./Data/PayerMlData.csv")
	td.LoadTrainingData(train_file, ''',''', 2, 0.25)
	print(td.EncodedTrainingData[0])
	print("Training Data Length: {} Testing Data Length: {}".format(len(td.TrainingX), len(td.TestingX)))
	print(td.TrainingX[0])
	print(td.TrainingY[0])

#Testing()
