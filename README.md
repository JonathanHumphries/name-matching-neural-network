First Release: January 28, 2019     Last Updated: February 5, 2019

This is a simple project showing how to create a Regression Neural Network for matching US Healthcare Insurance company names.  It can easily be adopted to other name lists such as author disambiguation.  It uses PyTorch as the neural network engine and references to important contributors.  This is a demo project, actual production code would contain significantly more features and probably be designed to run on a GPU instead of CPU for training.

In order to minimize the work on the neural network, a set of string comparision algorithms are employed.  Many of these are available in python libraries, but my preference is to code from scratch whenever it is efficient to do so.  This minimizes the impact of breaking changes in a production environment and improves developer understanding of what is being done.

Normally I would not commit data files; however, to make this shareable and to show the quality of data supplied (not the best, there are many errors), I have commited the data files as well.  I have been writing neural networks from scratch for quite some time; however, this is my first PyTorch implementation.  I used the work of James McCaffrey as a guide and cited his articles that were helpful.

This project is also a work in progress, as of right now, it is moving through the search space but more work can be done.

Change Log:
Feb 5, 2019
	Reworked accuracy calculation to model how a percent match would be evaluated by a human
	Added Termination Condition when 80% accuracy is achieved or 500,000 iterations have passed.
	Added code to Save the model after the termination condition is met.

Future Improvements include:
    	* Hyper Parameter Optimization
	* Min Max Scaling of encoded values
	* Evaluation of the loss function.  MSE did not work well but have I selected an ideal alternative?
	* Review of the string comparision algorithms and possibly using a primary feature analysis to remove any that are useless.
	* Operationalization of the neural network so that it is easily implemented in a production library
	* Other operationalization improvements such as a lookup table for known alternate spellings.
	* Load an existing model

Time invested to date: 7 hours, mostly dealing with PyTorch/Python syntax and data structures

Licensing:
  This software is available for use by anyone and for any purpose including commercial use.
  
Contact:
  Please feel free to contact me regarding this code or my private repositories.  My email address is Jon.Humphries79  yahoo dot com